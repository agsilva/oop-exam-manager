/**
 * Projeto de POO
 * 2015/2016
 */
package gestorexames;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Gerir o acesso a disciplinas
 *
 * @author Rita Pascoa
 */
public class Disciplina implements Serializable {

    private final int id;
    private final String nome;
    private final Docente resp;
    private final ArrayList<Docente> docentes;
    private final ArrayList<Aluno> alunos;

    /**
     * Construtor da classe Disciplina
     *
     * @param id ID da disciplina
     * @param nome Nome da disciplina
     * @param resp Docente responsável da disciplina
     * @param docentes Outros docentes da disciplina
     * @param alunos Alunos da disciplina
     */
    public Disciplina(int id, String nome, Docente resp, ArrayList<Docente> docentes, ArrayList<Aluno> alunos) {
        this.id = id;
        this.nome = nome;
        this.resp = resp;
        this.docentes = docentes;
        this.alunos = alunos;
    }

    /**
     * Devolve o ID da disciplina
     *
     * @return ID da disciplina
     */
    public int getId() {
        return id;
    }

    /**
     * Devolve o nome da disciplina
     *
     * @return Nome da disciplina
     */
    public String getNome() {
        return nome;
    }

    /**
     * Verifica se um determinado aluno está inscrito na disciplina
     *
     * @param nrAluno Número do aluno número de aluno a procurar
     * @return True, em caso afirmativo ou False, caso contrário
     */
    public boolean hasAluno(int nrAluno) {
        return this.alunos.stream().anyMatch((a) -> (a.getNrAluno() == nrAluno));
    }

    /**
     * Devolve dados da disciplina em formato String
     *
     * @return Dados do aluno
     */
    @Override
    public String toString() {
        return +this.id + "\t\t" + this.nome + "\t\t" + this.resp.getNome() + "\t\t" + this.docentes.size() + "\t\t" + this.alunos.size();
    }
}
