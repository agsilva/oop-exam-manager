/**
 * Projeto de POO
 * 2015/2016
 */
package gestorexames;

/**
 * Gerir o acesso a Funcionários
 *
 * @author Rita Pascoa
 */
public abstract class Funcionario extends Pessoa {

    protected int nrMecan;
    protected String categoria;

    public Funcionario(String nome, String email, int nrMec, String cat) {
        super(nome, email);
        this.nrMecan = nrMec;
        this.categoria = cat;
    }

    public int getNrMecan() {
        return nrMecan;
    }

}
