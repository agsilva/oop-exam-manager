/**
 * Projeto de POO
 * 2015/2016
 */
package gestorexames;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Gerir o acesso a ficheiros de objeto
 *
 * @author Rita Pascoa
 */
public class FicheiroObjeto {

    private ObjectInputStream iS;
    private ObjectOutputStream oS;

    public boolean abreLeitura(String nomeFich) {
        try {
            iS = new ObjectInputStream(new FileInputStream(nomeFich));
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public void abreEscrita(String nomeFich) {
        try {
            oS = new ObjectOutputStream(new FileOutputStream(nomeFich));
        } catch (IOException e) {
        }
    }

    public Object leObjeto() throws IOException, ClassNotFoundException {
        return iS.readObject();
    }

    public void escreveObjeto(Object o) throws IOException {
        oS.writeObject(o);
    }

    public void fechaLeitura() throws IOException {
        iS.close();
    }

    public void fechaEscrita() throws IOException {
        oS.close();
    }
}
