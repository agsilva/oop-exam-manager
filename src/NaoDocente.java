/**
 * Projeto de POO
 * 2015/2016
 */
package gestorexames;

/**
 *Gerir o acesso a Não Docentes
 * @author Rita Pascoa
 */
public class NaoDocente extends Funcionario {
    private final String cargo;

    public NaoDocente(String nome, String email, int nrMec, String cat, String cargo) {
        super(nome, email, nrMec, cat);
        this.cargo = cargo;
    }
    @Override
    public String toString(){
        return this.nrMecan+"\t\t"+this.nome+"\t"+this.categoria+"\t\t"+this.cargo;
    } 
    
}
