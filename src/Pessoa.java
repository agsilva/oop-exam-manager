/**
 * Projeto de POO
 * 2015/2016
 */
package gestorexames;

import java.io.Serializable;

/**
 * Gerir o acesso a Pessoas
 *
 * @author Rita Pascoa
 */
public abstract class Pessoa implements Serializable {

    protected String nome;
    protected String email;

    public Pessoa(String nome, String email) {
        this.nome = nome;
        this.email = email;
    }

    public String getNome() {
        return this.nome;
    }
}
