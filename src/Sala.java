/**
 * Projeto de POO
 * 2015/2016
 */
package gestorexames;

import java.io.Serializable;

/**
 * Gerir o acesso a Salas
 *
 * @author Rita Pascoa
 */
public class Sala implements Serializable {

    private final int id;
    private final String edificio;
    private final String espaco;
    private int nPiso;
    private int nPorta;
    private int nLugares;

    public Sala(int id, String edificio, String espaco) {
        this.id = id;
        this.edificio = edificio;
        this.espaco = espaco;
    }

    public Sala(int id, String edificio, String espaco, int nLugares) {
        this.id = id;
        this.edificio = edificio;
        this.espaco = espaco;
        this.nLugares = nLugares;
    }

    public Sala(int id, String edificio, String espaco, int nPiso, int nPorta) {
        this.id = id;
        this.edificio = edificio;
        this.espaco = espaco;
        this.nPiso = nPiso;
        this.nPorta = nPorta;
    }

    public Sala(int id, String edificio, String espaco, int nPiso, int nPorta, int nLugares) {
        this.id = id;
        this.edificio = edificio;
        this.espaco = espaco;
        this.nPiso = nPiso;
        this.nPorta = nPorta;
        this.nLugares = nLugares;
    }

    public int getId() {
        return this.id;
    }

    public String getInfo() {
        return this.edificio + ": " + this.espaco;
    }

    @Override
    public String toString() {
        String ret;

        ret = "Edifício: " + this.edificio + "; Espaço: " + this.espaco;

        if (nPiso > 0) {
            ret += "; Nº Piso: " + nPiso;
        }

        if (nPorta > 0) {
            ret += "; Nº Porta: " + nPorta;
        }

        if (nLugares > 0) {
            ret += "; Nº Lugares: " + nLugares;
        }

        return ret;
    }
}
