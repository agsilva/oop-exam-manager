/**
 * Projeto de POO
 * 2015/2016
 */
package gestorexames;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Gerir o acesso a Exames Especiais
 *
 * @author Rita Pascoa
 */
public class ExameEspecial extends Exame {

    /**
     * Construtor da classe ExameEspecial
     *
     * @param disciplina
     * @param data
     * @param duracao
     * @param sala
     * @param docente
     */
    public ExameEspecial(Disciplina disciplina, Date data, int duracao, Sala sala, Docente docente) {
        super(disciplina, data, duracao, sala, docente);
    }

    @Override
    public boolean validData(Date d, int tipoExame) {
        if (tipoExame == 3) {
            System.err.println("A disciplina " + disciplina.getNome() + " já tem um Exame Especial criado para este ano.");
            return false;
        }

        if (!data.after(d)) {
            if (tipoExame == 1) {
                System.err.println("Erro: O Exame Normal da disciplina " + disciplina.getNome() + " não pode ter uma data posterior à do Exame Especial já existente.");
            } else {
                System.err.println("Erro: O Exame de Recurso da disciplina " + disciplina.getNome() + " não pode ter uma data posterior à do Exame Especial já existente.");
            }

            return false;
        }

        return true;
    }

    @Override
    public boolean hasAdmissao(Aluno a) {
        if (!a.getCurso().hasDisciplina(this.disciplina.getId())) {
            return false;
        }

        if (a.getCurso().getDuracao() == a.getAnoMat()) {
            return true;
        }

        return !a.getRegime().equalsIgnoreCase("Normal");
    }

    @Override
    public String toString() {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        return "Especial\t\t" + this.disciplina.getNome() + "\t\t" + df.format(this.data)
                + "\t" + this.duracao + "\t\t" + this.sala.getInfo() + "\t" + this.docente.getNome()
                + "\t\t\t" + this.vigilantes.size() + "\t\t\t" + this.funcionarios.size()
                + "\t\t\t" + this.inscritos.size();
    }
}
