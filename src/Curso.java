/**
 * Projeto de POO
 * 2015/2016
 */
package gestorexames;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Gerir o acesso a cursos
 *
 * @author Rita Pascoa
 */
public class Curso implements Serializable {

    private final int id;
    private final String nome;
    private final int duracao;
    private final String grau;
    private final ArrayList<Disciplina> disciplinas;

    /**
     * Construtor da classe Curso
     *
     * @param id ID do curso
     * @param nome Nome do curso
     * @param duracao Duração em anos do curso
     * @param grau Grau que o curso confere
     */
    public Curso(int id, String nome, int duracao, String grau) {
        this.id = id;
        this.nome = nome;
        this.duracao = duracao;
        this.grau = grau;
        this.disciplinas = new ArrayList<>();
    }

    /**
     * Devolve o ID do curso
     *
     * @return ID do curso
     */
    public int getId() {
        return this.id;
    }

    /**
     * Devolve o nome do curso
     *
     * @return Nome do curso
     */
    public String getNome() {
        return this.nome;
    }

    /**
     * Devolve a duração do curso
     *
     * @return Duração do curso
     */
    public int getDuracao() {
        return this.duracao;
    }

    /**
     * Devolve o grau do curso
     *
     * @return Grau do curso
     */
    public String getGrau() {
        return this.grau;
    }

    /**
     * Verifica se um determinado ID de uma discipina existe numa das
     * disciplinas do curso
     *
     * @param id ID de uma disciplina
     * @return True, em caso afirmativo; False, caso contrário
     */
    public boolean hasDisciplina(int id) {
        return this.disciplinas.stream().anyMatch((d) -> (d.getId() == id));
    }

    /* testes */
    /**
     * Adiciona uma disciplina ao curso
     *
     * @param d Disciplina
     */
    public void setDisciplina(Disciplina d) {
        this.disciplinas.add(d);
    }
}
