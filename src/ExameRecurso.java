/**
 * Projeto de POO
 * 2015/2016
 */
package gestorexames;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Gerir o acesso a Exames de Recurso
 *
 * @author Rita Pascoa
 */
public class ExameRecurso extends Exame {

    public ExameRecurso(Disciplina disciplina, Date data, int duracao, Sala sala, Docente docente) {
        super(disciplina, data, duracao, sala, docente);
    }

    @Override
    public boolean validData(Date d, int tipoExame) {
        if (tipoExame == 2) {
            System.err.println("A disciplina " + disciplina.getNome() + " já tem um Exame de Recurso criado para este ano.");
            return false;
        }

        if (tipoExame == 1 && !data.after(d)) {
            System.err.println("Erro: O Exame Normal da disciplina " + disciplina.getNome() + " não pode ter uma data posterior à do Exame de Recurso já existente.");
            return false;
        }

        if (tipoExame == 3 && !data.before(d)) {
            System.err.println("Erro: O Exame Especial da disciplina " + disciplina.getNome() + " não pode ter uma data anterior à do Exame de Recurso já existente.");
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        return "Recurso\t\t" + this.disciplina.getNome() + "\t\t" + df.format(this.data)
                + "\t" + this.duracao + "\t\t" + this.sala.getInfo() + "\t" + this.docente.getNome()
                + "\t\t\t" + this.vigilantes.size() + "\t\t\t" + this.funcionarios.size()
                + "\t\t\t" + this.inscritos.size();
    }

    @Override
    public boolean hasAdmissao(Aluno a) {
        return a.getCurso().hasDisciplina(this.disciplina.getId());
    }
}
