/**
 * Projeto de POO
 * 2015/2016
 */
package gestorexames;

/**
 * Gerir o acesso a docentes
 *
 * @author Rita Pascoa
 */
public class Docente extends Funcionario {

    private final String areaInvest;

    /**
     * Construtor da classe Docente
     *
     * @param nome Nome do docente
     * @param email Email do docente
     * @param nrMec Número mecanográfico do docente
     * @param cat Categoria do docente
     * @param area Área de investigação do docente
     */
    public Docente(String nome, String email, int nrMec, String cat, String area) {
        super(nome, email, nrMec, cat);
        this.areaInvest = area;
    }

    /**
     * Devolve dados do docente em formato String
     *
     * @return Dados
     */
    @Override
    public String toString() {
        return this.nrMecan + "\t\t" + this.nome + "\t\t" + this.categoria + "\t\t" + this.areaInvest;
    }
}
