/**
 * Projeto de POO
 * 2015/2016
 */
package gestorexames;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Gerir o acesso a ficheiros de texto
 * @author Rita Pascoa
 */
public class FicheiroTexto {
    /**
     * 
     */
    private BufferedReader fR;
    private BufferedWriter fW;
    
    /**
     * Método para a abrir o ficheiro de texto para leitura
     * @param nomeDoFicheiro Nome do ficheiro
     * @throws FileNotFoundException Caso não seja possível encontrar o ficheiro
     */
    public void abreLeitura(String nomeDoFicheiro) throws FileNotFoundException
    {
        fR = new BufferedReader(new FileReader(nomeDoFicheiro));
    }
    
    public void abreEscrita(String nomeDoFicheiro) throws IOException
    {
        fW = new BufferedWriter(new FileWriter(nomeDoFicheiro));
    }
    
    /**
     * Método para ler uma linha do ficheiro de texto
     * @return Linha lida
     * @throws IOException Caso não seja possível ler uma linha
     */
    public String leLinha() throws IOException
    {
        return fR.readLine();
    }
    
    public void escreveLinha(String linha) throws IOException
    {
        fW.write(linha,0,linha.length());
        fW.newLine();
    }
    
    public void fechaLeitura() throws IOException {
        fR.close();
    }

    public void fechaEscrita() throws IOException {
        fW.close();
    }
}
