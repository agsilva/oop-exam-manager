/**
 * Projeto de POO
 * 2015/2016
 */
package gestorexames;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Gerir o acesso a Exames Normais
 *
 * @author Rita Pascoa
 */
public class ExameNormal extends Exame {

    public ExameNormal(Disciplina disciplina, Date data, int duracao, Sala sala, Docente docente) {
        super(disciplina, data, duracao, sala, docente);
    }

    @Override
    public boolean validData(Date d, int tipoExame) {
        if (tipoExame == 1) {
            System.err.println("Erro: A disciplina " + disciplina.getNome() + " já tem um Exame Normal criado para este ano.");
            return false;
        }

        if (!data.before(d)) {
            if (tipoExame == 2) {
                System.err.println("Erro: O Exame de Recurso da disciplina " + disciplina.getNome() + " não pode ter uma data anterior à do Exame Normal já existente.");
            } else {
                System.err.println("Erro: O Exame Especial da disciplina " + disciplina.getNome() + " não pode ter uma data anterior à do Exame Normal já existente.");
            }

            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        return "Normal\t\t" + this.disciplina.getNome() + "\t\t" + df.format(this.data)
                + "\t" + this.duracao + "\t\t" + this.sala.getInfo() + "\t" + this.docente.getNome()
                + "\t\t\t" + this.vigilantes.size() + "\t\t\t" + this.funcionarios.size()
                + "\t\t\t" + this.inscritos.size();
    }

    @Override
    public boolean hasAdmissao(Aluno a) {
        return a.getCurso().hasDisciplina(this.disciplina.getId());
    }
}
