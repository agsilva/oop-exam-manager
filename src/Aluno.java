/**
 * Projeto de POO
 * 2015/2016
 */
package gestorexames;

/**
 * Gerir o acesso a alunos
 *
 * @author Rita Pascoa
 */
public class Aluno extends Pessoa {

    private final int nrAluno;
    private final Curso curso;
    private final int anoMat;
    private final String regime;

    /**
     * Construtor da classe Aluno
     *
     * @param nome Nome do aluno
     * @param email Email do aluno
     * @param nrAluno Número de aluno
     * @param curso Curso do aluno
     * @param ano Ano de matrícula do aluno
     * @param reg Regime do aluno
     */
    public Aluno(String nome, String email, int nrAluno, Curso curso, int ano, String reg) {
        super(nome, email);
        this.nrAluno = nrAluno;
        this.curso = curso;
        this.anoMat = ano;
        this.regime = reg;
    }

    /**
     * Devolve o número de aluno
     *
     * @return Número de aluno
     */
    public int getNrAluno() {
        return nrAluno;
    }

    /**
     * Devolve o ano de matrícula do aluno
     *
     * @return Ano de Matrícula
     */
    public int getAnoMat() {
        return anoMat;
    }

    /**
     * Devolve o regime do aluno
     *
     * @return Regime
     */
    public String getRegime() {
        return regime;
    }

    /**
     * Devolve o Curso do aluno
     *
     * @return Elemento 'Curso'
     */
    public Curso getCurso() {
        return this.curso;
    }

    /**
     * Devolve dados do aluno em formato String
     *
     * @return Dados do aluno
     */
    @Override
    public String toString() {
        return this.nrAluno + "\t\t" + this.nome + "\t\t" + this.curso.getNome() + "\t\t" + this.anoMat + "\t\t" + this.regime;
    }

}
