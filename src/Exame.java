/**
 * Projeto de POO
 * 2015/2016
 */
package gestorexames;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Gerir o acesso a exames
 *
 * @author Rita Pascoa
 */
public abstract class Exame implements Serializable {

    protected Disciplina disciplina;
    protected Date data;
    protected int duracao;
    protected Sala sala;
    protected Docente docente;
    protected ArrayList<Docente> vigilantes;
    protected ArrayList<NaoDocente> funcionarios;
    protected Map<Aluno, Float> inscritos;

    /**
     * Construtor da classe Exame
     *
     * @param disciplina Disciplina do exame
     * @param data Data do exame
     * @param duracao Duração do exame
     * @param sala Sala do exame
     * @param docente Docente responsável do exame
     */
    public Exame(Disciplina disciplina, Date data, int duracao, Sala sala, Docente docente) {
        this.disciplina = disciplina;
        this.data = data;
        this.duracao = duracao;
        this.sala = sala;
        this.docente = docente;
        this.vigilantes = new ArrayList<>();
        this.funcionarios = new ArrayList<>();
        this.inscritos = new HashMap<>(/*(Aluno a1, Aluno a2) -> (a1.getNome()).compareTo(a2.getNome()*/);
    }

    /**
     * Dovolve a disciplina do exame
     *
     * @return Elemento 'Disciplina'
     */
    public Disciplina getDisciplina() {
        return this.disciplina;
    }

    /**
     * Devolve a data do exame
     *
     * @return Elemento 'Date'
     */
    public Date getData() {
        return this.data;
    }

    /**
     * Devolve a duração do exame
     *
     * @return Duração do exame
     */
    public int getDuracao() {
        return this.duracao;
    }

    /**
     * Devolve a sala do do exame
     *
     * @return Elemento 'Sala'
     */
    public Sala getSala() {
        return this.sala;
    }

    /**
     * Adiciona um vigilante ao exame
     *
     * @param d Docente
     */
    public void addVigilante(Docente d) {
        this.vigilantes.add(d);
    }

    /**
     * Adiciona um funcionário ao exame
     *
     * @param nd Não docente
     */
    public void addFuncionario(NaoDocente nd) {
        this.funcionarios.add(nd);
    }

    /**
     * Verifica se o vigilante já foi convocado
     *
     * @param d Docente
     * @return True,em caso afirmativo ou False, caso contrário
     */
    public boolean hasVigilante(Docente d) {
        return this.vigilantes.stream().anyMatch((doc) -> (d.getNrMecan() == doc.getNrMecan()));
    }

    /**
     * Verifica se um funcionário já foi convocado
     *
     * @param nd Não docente
     * @return True,em caso afirmativo ou False, caso contrário
     */
    public boolean hasFuncionario(NaoDocente nd) {
        return this.funcionarios.stream().anyMatch((ndoc) -> (nd.getNrMecan() == ndoc.getNrMecan()));
    }

    /**
     * Devolve o conjunto de alunos inscritos e classificações respetivas
     *
     * @return Conjunto Aluno-Classificação
     */
    public Set<Entry<Aluno, Float>> getInscritos() {
        return this.inscritos.entrySet();
    }

    /**
     * Verifica se um aluno já está inscrito e indica a sua classificação, caso
     * haja
     *
     * @param nrAluno Número de aluno
     * @return -2: Não inscrito, -1: Inscrito e sem nota, Nota: inscrito e com
     * nota
     */
    public float hasAluno(int nrAluno) {
        Float nota;

        for (Aluno a : this.inscritos.keySet()) {
            if (a.getNrAluno() == nrAluno) {
                if ((nota = this.inscritos.get(a)) == null) {
                    return -1;
                }
                return nota;
            }
        }
        return -2;
    }

    /**
     * Adiciona um aluno ao exame
     *
     * @param a Aluno
     */
    public void addAluno(Aluno a) {
        this.inscritos.put(a, null);
    }

    /**
     * Verifica se existem notas
     *
     * @return True, em caso afirmativo ou False, caso contrário
     */
    public boolean hasNotas() {
        return this.inscritos.values().stream().anyMatch((val) -> (val != null));
    }

    /**
     * Verifica se uma data é válida para criar exame
     *
     * @param d Data
     * @param tipoExame Época
     * @return True, em caso afirmativo ou False, caso contrário
     */
    public abstract boolean validData(Date d, int tipoExame);

    /**
     * Verifica se o exame está sobreposto, em horário, com um prestes a criar
     *
     * @param d Data
     * @param dur Duração
     * @return True, em caso afirmativo ou False, caso contrário
     */
    public boolean isSobreposto(Date d, int dur) {
        long iExame = this.data.getTime();
        long fExame = this.data.getTime() + this.duracao * 60000;
        long iExameNovo = d.getTime();
        long fExameNovo = d.getTime() + dur * 60000;

        if (iExameNovo <= iExame && fExameNovo > iExame) {
            return true;
        }

        if (iExameNovo >= iExame && iExameNovo < fExame) {
            return true;
        }

        return fExameNovo >= iExame && fExameNovo <= fExame;
    }

    /**
     * Verifica se o aluno é admissível a exame
     *
     * @param a Aluno
     * @return True, em caso afirmativo ou False, caso contrário
     */
    public abstract boolean hasAdmissao(Aluno a);

    /**
     * Verifica se uma determinada sala está ocupada
     *
     * @param id ID da sala
     * @return True, em caso afirmativo ou False, caso contrário
     */
    public boolean hasSala(int id) {
        return this.sala.getId() == id;
    }

    /**
     * Verifica se um vigilante já foi convocado
     *
     * @param nrMec Número mecanográfico do docente
     * @return True, em caso afirmativo ou False, caso contrário
     */
    public boolean hasDocente(int nrMec) {
        return this.vigilantes.stream().anyMatch((d) -> (d.getNrMecan() == nrMec));
    }

    /**
     * Verifica se um funcionário já foi convocado
     *
     * @param nrMec Número mecanográfico do docente
     * @return True, em caso afirmativo ou False, caso contrário
     */
    public boolean hasNaoDocente(int nrMec) {
        return this.funcionarios.stream().anyMatch((nd) -> (nd.getNrMecan() == nrMec));
    }

    /**
     * Devolve os vigilantes do exame
     *
     * @return Vigilantes
     */
    public ArrayList<Docente> getVigilantes() {
        return this.vigilantes;
    }

    /**
     * Devolve os funcionários do exame
     *
     * @return Funcionários
     */
    public ArrayList<NaoDocente> getFuncionarios() {
        return this.funcionarios;
    }

    /**
     * Adiciona classificação a um aluno
     *
     * @param opcao Posição do aluno na lista
     * @param valor Nota
     */
    void addNota(int opcao, Float valor) {
        int i = 1;
        for (Entry<Aluno, Float> map : this.inscritos.entrySet()) {
            if (i == opcao) {
                map.setValue(valor);
                break;
            }
            i++;
        }
    }
}
