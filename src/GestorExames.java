/**
 * Projeto de POO
 * 2015/2016
 */
package gestorexames;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * Gerir o acesso a todos os dados da aplicação
 *
 * @author Rita Pascoa
 */
public class GestorExames {

    static ArrayList<Aluno> alunos = new ArrayList<>();
    static ArrayList<Docente> docentes = new ArrayList<>();
    static ArrayList<NaoDocente> naoDocentes = new ArrayList<>();
    static ArrayList<Curso> cursos = new ArrayList<>();
    static ArrayList<Disciplina> disciplinas = new ArrayList<>();
    static ArrayList<Exame> exames = new ArrayList<>();
    static ArrayList<Sala> salas = new ArrayList<>();

    static Scanner sc = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        boolean debug = false;
        /*Testes*/

        if (debug) {
            carregaTestes();
        } else {
            /*Carrega ficheiros de texto*/
            carregaFicheiroDocentes("Docentes.txt");
            if (docentes.isEmpty()) {
                System.err.println("Erro: Ficheiro de Docentes vazio ou inexistente. O programa vai terminar.");
                return;
            }
            carregaFicheiroNaoDocentes("NaoDocentes.txt");
            if (naoDocentes.isEmpty()) {
                System.err.println("Erro: Ficheiro de Não Docentes vazio ou inexistente. O programa vai terminar.");
                return;
            }

            /*Carrega ficheiros de objeto*/
            cursos = (ArrayList<Curso>) carregaFicheiroObjeto("Cursos.dat");
            if (cursos == null || cursos.isEmpty()) {
                System.err.println("Erro: Ficheiro de Cursos vazio ou inexistente. O programa vai terminar.");
                return;
            }

            carregaFicheiroAlunos("Alunos.txt");
            if (alunos.isEmpty()) {
                System.err.println("Erro: Ficheiro de Alunos vazio ou inexistente. O programa vai terminar.");
                return;
            }
            disciplinas = (ArrayList<Disciplina>) carregaFicheiroObjeto("Disciplinas.dat");
            if (disciplinas == null || disciplinas.isEmpty()) {
                System.err.println("Erro: Ficheiro de Diciplinas vazio ou inexistente. O programa vai terminar.");
                return;
            }
            salas = (ArrayList<Sala>) carregaFicheiroObjeto("Salas.dat");
            if (salas == null || salas.isEmpty()) {
                System.err.println("Erro: Ficheiro de Salas vazio ou inexistente. O programa vai terminar.");
                return;
            }
        }
        exames = (ArrayList<Exame>) carregaFicheiroObjeto("Exames.dat");
        if (exames == null) {
            exames = new ArrayList<>();
        }

        menuPrincipal();

        System.out.println("Bye bye.");
    }

    private static void menuPrincipal() {
        String optStr;
        int opcao;

        while (true) {
            System.out.println("\n.:Menu:.\n"
                    + "1. Criar exame\n"
                    + "2. Remover exame\n"
                    + "3. Convocar vigilantes\n"
                    + "4. Convocar funcionários\n"
                    + "5. Inscrever alunos\n"
                    + "6. Lançar notas de um exame\n"
                    + "7. Listar exames\n"
                    + "8. Listar alunos inscritos num exame\n"
                    + "9. Listar exames de um aluno\n"
                    + "10. Listar funcionários associados a um exame\n"
                    + "11. Listar exames em que um funcionário está envolvido\n"
                    + "12. Listar notas de um exame\n"
                    + "0. Sair\n\n"
                    + "Escolha uma opção: ");
            optStr = sc.nextLine();
            if (!verificaNumero(optStr)) {
                System.err.println("Erro: Opção tem de ser um número!");
            } else {
                opcao = Integer.parseInt(optStr);

                switch (opcao) {
                    case 1:
                        criaExame();
                        break;

                    case 2:
                        removeExame();
                        break;
                    case 3:
                        convocaVigilantes(null, null);
                        break;

                    case 4:
                        convocaFuncionarios(null);
                        break;

                    case 5:
                        inscreveAlunos();
                        break;

                    case 6:
                        lancaNotas();
                        break;

                    case 7:
                        listaExames();
                        break;

                    case 8:
                        listaAlunosExame();
                        break;

                    case 9:
                        listaExamesAluno();
                        break;

                    case 10:
                        listaFuncionariosExame();
                        break;

                    case 11:
                        listaExamesFuncionario();
                        break;

                    case 12:
                        listaNotasExame();
                        break;

                    case 0:
                        return;

                    default:
                        System.err.println("Erro: Opção inválida!");
                        break;

                }
            }
        }
    }

    /**
     * Método que cria um exame
     */
    private static void criaExame() {
        String optStr;
        int duracao, tipoExame;
        Disciplina disc;
        Date data;
        Sala sala;
        Docente doc;

        System.out.println("\n============ Criar Exame ============");

        if ((disc = selecionaDiciplina()) == null) {
            saiMenuAtual();
            return;
        }

        while (true) {
            if ((tipoExame = selecionaTipoExame()) == 0) {
                return;
            }
            if ((data = selecionaData()) == null) {
                return;
            }

            if (verificaDataExame(disc.getId(), data, tipoExame)) {
                break;
            }

            System.out.println("Prima 0 para sair ou outra tecla para tentar de novo.");
            optStr = sc.nextLine();
            if (optStr.startsWith("0")) {
                return;
            }
        }

        while (true) {
            System.out.println("Duração [min]: ");
            optStr = sc.nextLine();

            if (!verificaNumero(optStr)) {
                System.err.println("Erro: Duração tem de ser um número (inteiro)!\nPrima 0 se pretende sair da configuração.");
            } else {
                duracao = Integer.parseInt(optStr);
                if (duracao == 0) {
                    return;
                }
                if (duracao > 300) {
                    System.err.println("Erro: A duração não pode ser superior a 300 minutos!");
                } else {
                    break;
                }
            }
        }

        ArrayList<Exame> examesSobrepostos;

        examesSobrepostos = getExamesSobrepostos(data, duracao);

        if ((sala = selecionaSala(examesSobrepostos)) == null) {
            return;
        }

        if ((doc = selecionaDocente("Docente Responsável")) == null) {
            return;
        }

        Exame novo;
        if (tipoExame == 1) {
            novo = new ExameNormal(disc, data, duracao, sala, doc);
        } else {
            if (tipoExame == 2) {
                novo = new ExameRecurso(disc, data, duracao, sala, doc);
            } else {
                novo = new ExameEspecial(disc, data, duracao, sala, doc);
            }
        }
        convocaVigilantes(novo, examesSobrepostos);

        if (!novo.getVigilantes().isEmpty()) {
            convocaFuncionarios(novo);
            saiMenuAtual();
        }

    }

    /**
     * Método que remove um exame da lista de exames
     */
    private static void removeExame() {
        String optStr;

        System.out.println("\n============ Remover Exame ============");

        Exame e = selecionaExame();

        if (e == null) {
            saiMenuAtual();
            return;
        }

        if (e.hasNotas()) {
            System.err.println("Erro: Não é possível remover exames com notas já lançadas!");
            saiMenuAtual();
            return;
        }

        while (true) {
            System.out.println("Tem a certeza que pretende remover este exame [s/n]?\n[" + e + "]");
            optStr = sc.nextLine();
            if (optStr.equalsIgnoreCase("n")) {
                return;
            }
            if (optStr.equalsIgnoreCase("s")) {
                exames.remove(e);
                atualizaFicheiroObjeto("Exames.dat", (Object) exames);
                System.out.println("Exame removido com sucesso!");
                saiMenuAtual();
                return;
            }

            System.err.println("Erro: Opção inválida!");
        }
    }

    private static void convocaVigilantes(Exame novo, ArrayList<Exame> examesSobrepostos) {
        int opcao = 0, i = 0;
        String optStr;
        ArrayList<Integer> vigsDisponiveisInd = new ArrayList<>();
        Docente doc;
        boolean free, check = true;

        //Se vier da criação de exame
        if (novo != null) {
            System.out.println("\n.:Vigilantes Disponíveis:.");

            for (Docente d : docentes) {
                free = true;
                for (Exame e : examesSobrepostos) {
                    if (e.hasDocente(d.getNrMecan())) {
                        free = false;
                        break;
                    }
                }
                if (free) {
                    if (check) {
                        check = false;
                        System.out.println("  Nº Mencanográfico\t\t\tNome\t\t\tCategoria\t\tÁrea de Investigação");
                    }
                    vigsDisponiveisInd.add(docentes.indexOf(d));
                    opcao++;
                    System.out.println(opcao + ". " + d);
                }
            }

            if (opcao == 0) {
                System.err.println("Não existem vigilantes disponíveis para esta hora.Para alterar a data, crie de novo o exame.");
                saiMenuAtual();
                return;
            }
            System.out.println("Escolha pelo menos UMA opção [0. Sair]: ");
            while (true) {
                i++;
                while (true) {
                    System.out.println("Docente " + i + ": ");
                    optStr = sc.nextLine();
                    if (!verificaNumero(optStr)) {
                        System.err.println("Erro: Opção tem de ser um número!");
                    } else {
                        opcao = Integer.parseInt(optStr);
                        if (opcao > 0 && opcao <= vigsDisponiveisInd.size()) {
                            doc = docentes.get(vigsDisponiveisInd.get(opcao - 1));
                            if (!novo.hasDocente(doc.getNrMecan())) {
                                novo.addVigilante(doc);
                                if (i == 1) {
                                    adicionaExame(novo);
                                    System.out.println("Exame criado com sucesso!");
                                }
                                atualizaFicheiroObjeto("Exames.dat", (Object) exames);
                                System.out.println("Docente " + doc.getNome() + " convocado com sucesso.");
                                break;
                            } else {
                                System.err.println("Erro: Docente " + doc.getNome() + " já foi convocado.");
                            }

                        } else {
                            if (opcao == 0) {
                                return;
                            } else {
                                System.err.println("Erro: Opção inválida!");
                            }
                        }
                    }
                }
            }
        } else {
            System.out.println("\n============ Convocar Vigilantes ============");

            Exame ex = selecionaExame();
            if (ex == null) {
                saiMenuAtual();
                return;
            }

            System.out.println("\n.:Vigilantes Disponíveis:.");

            examesSobrepostos = getExamesSobrepostos(ex.getData(), ex.getDuracao());
            for (Docente d : docentes) {
                free = true;
                for (Exame e : examesSobrepostos) {
                    if (e.hasDocente(d.getNrMecan())) {
                        free = false;
                        break;
                    }
                }
                if (free) {
                    if (check) {
                        check = false;
                        System.out.println("  Nº Mencanográfico\t\t\tNome\t\t\tCategoria\t\tÁrea de Investigação");
                    }
                    vigsDisponiveisInd.add(docentes.indexOf(d));
                    opcao++;
                    System.out.println(opcao + ". " + d);
                }
            }

            if (opcao == 0) {
                System.err.println("Não existem mais vigilantes disponíveis para esta hora.");
                saiMenuAtual();
                return;
            }
            System.out.println("Escolha uma opção [0. Sair]: ");
            while (true) {
                i++;
                while (true) {
                    System.out.println("Docente " + i + ": ");
                    optStr = sc.nextLine();
                    if (!verificaNumero(optStr)) {
                        System.err.println("Erro: Opção tem de ser um número!");
                    } else {
                        opcao = Integer.parseInt(optStr);
                        if (opcao > 0 && opcao <= vigsDisponiveisInd.size()) {
                            doc = docentes.get(vigsDisponiveisInd.get(opcao - 1));
                            if (!ex.hasDocente(doc.getNrMecan())) {
                                ex.addVigilante(doc);
                                atualizaFicheiroObjeto("Exames.dat", (Object) exames);
                                System.out.println("Docente " + doc.getNome() + " convocado com sucesso.");
                                break;
                            } else {
                                System.err.println("Erro: Docente " + doc.getNome() + " já foi convocado.");
                            }

                        } else {
                            if (opcao == 0) {
                                return;
                            } else {
                                System.err.println("Erro: Opção inválida!");
                            }
                        }
                    }
                }
            }
        }
    }

    private static void convocaFuncionarios(Exame e) {
        int opcao = 0, i = 0;
        String optStr;
        NaoDocente nDoc;

        //Se vier da criação de exame
        if (e == null) {
            System.out.println("\n============ Convocar Funcionários ============");
            e = selecionaExame();
            if (e == null) {
                saiMenuAtual();
                return;
            }
        }
        System.out.println("\n.:Funcionários:.");

        if (naoDocentes.isEmpty()) {
            System.out.println("Não existem funcionários não docentes na base de dados.");
            saiMenuAtual();
            return;
        }
        System.out.println("  Nº Mencanográfico\t\t\tNome\t\t\tCategoria\t\t\tCargo");
        for (NaoDocente nd : naoDocentes) {
            opcao++;
            System.out.println(opcao + ". " + nd);
        }

        System.out.println("Escolha uma opção [0. Sair]: ");
        while (true) {
            i++;
            while (true) {
                System.out.println("Não Docente " + i + ": ");
                optStr = sc.nextLine();
                if (!verificaNumero(optStr)) {
                    System.err.println("Erro: Opção tem de ser um número!");
                } else {
                    opcao = Integer.parseInt(optStr);
                    if (opcao > 0 && opcao <= naoDocentes.size()) {
                        nDoc = naoDocentes.get(opcao - 1);
                        if (!e.hasFuncionario(nDoc)) {
                            e.addFuncionario(nDoc);
                            atualizaFicheiroObjeto("Exames.dat", (Object) exames);
                            System.out.println("Não Docente " + nDoc.getNome() + " convocado com sucesso.");
                            break;
                        } else {
                            System.err.println("Erro: Não Docente " + nDoc.getNome() + " já foi convocado.");
                        }
                    } else {
                        if (opcao == 0) {
                            return;
                        } else {
                            System.err.println("Erro: Opção inválida!");
                        }
                    }
                }
            }
        }
    }

    /**
     * Método que inscreve um aluno num exame
     */
    private static void inscreveAlunos() {
        Aluno a;
        Exame e;

        System.out.println("\n============ Inscrever Alunos ============");

        if ((e = selecionaExame()) != null) {

            if (e.hasNotas()) {
                System.err.println("Erro: Notas já lançadas!");
            } else {

                while (true) {
                    if ((a = selecionaAlunoExame(e)) == null) {
                        return;
                    }

                    if (e.hasAluno(a.getNrAluno()) == -2) {
                        e.addAluno(a);
                        atualizaFicheiroObjeto("Exames.dat", (Object) exames);
                        System.out.println("Aluno(a) " + a.getNome() + " inscrito(a) com sucesso!");
                    } else {
                        System.err.println("Erro: Aluno(a) " + a.getNome() + " já inscrito(a).");
                    }
                }
            }
        }

        saiMenuAtual();
    }

    /**
     * Método que atribui classificações dos alunos num exame
     */
    private static void lancaNotas() {
        String optStr;
        int opcao, i;
        float opt;
        Exame e;

        System.out.println("\n============ Lançar Notas ============");

        if ((e = selecionaExame()) != null) {
            if (e.getInscritos().isEmpty()) {
                System.out.println("Não há alunos inscritos neste exame...");
            } else {
                System.out.println("  Nº Aluno\t\t\tNome\t\t\tClassificação");

                while (true) {
                    i = 0;
                    for (Map.Entry<Aluno, Float> map : e.getInscritos()) {
                        i++;
                        if (map.getValue() == null) {
                            System.out.println(i + ". " + map.getKey().getNrAluno() + "\t\t" + map.getKey().getNome());
                        } else {
                            System.out.println(i + ". " + map.getKey().getNrAluno() + "\t\t" + map.getKey().getNome() + "\t\t" + map.getValue());
                        }
                    }
                    System.out.println("Escolha uma opção [0. Sair]");
                    optStr = sc.nextLine();
                    if (!verificaNumero(optStr)) {
                        System.err.println("Erro: Opção tem de ser um número!");
                    } else {
                        opcao = Integer.parseInt(optStr);
                        if (opcao > 0 && opcao <= e.getInscritos().size()) {
                            while (true) {
                                System.out.println("Nota [0.0 a 20.0]: ");
                                optStr = sc.nextLine();
                                if (!verificaFloat(optStr)) {
                                    System.err.println("Erro: Opção tem de ser um número (decimal)!");
                                } else {
                                    opt = Float.parseFloat(optStr);

                                    if (opt >= 0 && opt <= 20) {
                                        e.addNota(opcao, opt);
                                        atualizaFicheiroObjeto("Exames.dat", (Object) exames);
                                        System.out.println("Nota introduzida com sucesso!");
                                        break;
                                    }
                                }
                            }
                        } else {
                            if (opcao == 0) {
                                return;
                            } else {
                                System.err.println("Erro: Opção inválida!");
                            }

                        }
                    }
                }
            }
        }
        saiMenuAtual();

    }

    /**
     * Método que lista todos os exames criados
     */
    private static void listaExames() {
        String str, linha;
        int i = 0;

        System.out.println("\n============ Listar Exames ============");

        if (exames.isEmpty()) {
            System.out.println("Não existem exames na base de dados...");
        } else {
            str = "    Época\t    Disciplina\t\t    Data\t  Duração (min)\t\tSala\t\tDocente Responsável\t\t\tNº de Vigilantes\tNº de Funcionários\tNº de Inscritos\n";
            System.out.print(str);
            for (Exame e : exames) {
                i++;
                linha = i + ". " + e;
                System.out.println(linha);
                str += linha + "\n";
            }
            guardaInfo(str);
        }

        saiMenuAtual();
    }

    /**
     * Método que lista todos os alunos inscritos num exame e respetiva
     * classificação, caso exista
     */
    private static void listaAlunosExame() {
        String str, linha;

        System.out.println("\n============ Listar Alunos de um Exame ============");

        Exame e = selecionaExame();
        if (e != null) {
            if (e.getInscritos().isEmpty()) {
                System.out.println("Não há alunos inscritos para este exame...");
            } else {
                str = "Nº Aluno\t\t\tNome\t\t\tClassificação\n";
                System.out.print(str);
                for (Map.Entry<Aluno, Float> map : e.getInscritos()) {
                    if (map.getValue() == null) {
                        linha = map.getKey().getNrAluno() + "\t" + map.getKey().getNome();
                        System.out.println(linha);
                    } else {
                        linha = map.getKey().getNrAluno() + "\t" + map.getKey().getNome() + "\t\t" + map.getValue();
                        System.out.println(linha);
                    }
                    str += linha + "\n";
                }

                guardaInfo(str);
            }
        }
        saiMenuAtual();
    }

    /**
     * Método que lista todos os exames a que um aluno está incrito
     */
    private static void listaExamesAluno() {
        int i = 0;
        float resultado;
        String str, linha;
        Aluno a = selecionaAluno();

        System.out.println("\n============ Listar Exames de um Aluno ============");

        if (a != null) {
            str = ".:Exames de " + a.getNome() + ":.\n";
            System.out.print(str);
            for (Exame e : exames) {
                if ((resultado = e.hasAluno(a.getNrAluno())) > -2) {
                    i++;
                    if (i == 1) {
                        linha = "    Época\t    Disciplina\t\t    Data\t  Duração (min)\t\tSala\t\tDocente Responsável\t\t\tNº de Vigilantes\tNº de Funcionários\tNº de Inscritos\t\tClassificação\n";
                        System.out.print(linha);
                        str += linha + "\n";
                    }
                    if (resultado == -1) {
                        linha = i + ". " + e;
                        System.out.println(linha);
                    } else {
                        linha = i + ". " + e + "\t\t\t" + resultado;
                        System.out.println(linha);
                    }
                    str += linha + "\n";
                }
            }
            if (i == 0) {
                System.out.println(a.getNome() + " não está inscrito(a) em nenhum exame.");
            } else {
                guardaInfo(str);
            }
        }
        saiMenuAtual();
    }

    /**
     * Método que lista todos os funcionários (docentes e não docentes)
     * convocados para um exame
     */
    private static void listaFuncionariosExame() {
        String str, linha;
        Exame e;

        System.out.println("\n============ Listar Funcionários de um Exame ============");

        if ((e = selecionaExame()) == null) {
            saiMenuAtual();
            return;
        }

        str = ".:Exame: [" + e + "]:.\n";
        System.out.print(str);
        linha = "\n.:Vigilantes:.\nNº Mencanográfico\t\t\tNome\t\t\tCategoria\t\tÁrea de Investigação";
        System.out.println(linha);
        str += linha + "\n";
        for (Docente v : e.getVigilantes()) {
            linha = v.toString();
            System.out.println(linha);
            str += linha + "\n";
        }

        linha = "\n.:Funcionários:.\nNº Mencanográfico\t\t\tNome\t\t\tCategoria\t\t\tCargo";
        System.out.println(linha);
        for (NaoDocente f : e.getFuncionarios()) {
            linha = f.toString();
            System.out.println(linha);
            str += linha + "\n";
        }

        guardaInfo(str);
        saiMenuAtual();

    }

    /**
     * Método menú para escolher que tipo de funcionário (docente ou não
     * docente) terá as suas convocatórias listadas
     */
    private static void listaExamesFuncionario() {
        int opcao;
        String optStr;

        while (true) {
            System.out.println("\n============ Listar Exames de um Funcionário ============");

            System.out.println(".:Funcionários:.\n"
                    + "1. Docentes\n"
                    + "2. Não Docentes\n"
                    + "0. Sair\n\n"
                    + "Escolha uma opção: ");
            optStr = sc.nextLine();
            if (!verificaNumero(optStr)) {
                System.err.println("Erro: Opção tem de ser um número!");
            } else {
                opcao = Integer.parseInt(optStr);
                switch (opcao) {
                    case 1:
                        listaExamesDocente();
                        break;

                    case 2:
                        listaExamesNaoDocente();
                        break;

                    case 0:
                        return;

                    default:
                        System.err.println("Erro: Opcão inválida!");
                        break;
                }
            }
        }
    }

    /**
     * Método que lista exames de um docente
     */
    private static void listaExamesDocente() {
        int i = 0;
        Docente d;
        String str, linha;

        System.out.println("\n============ Listar Exames de um Vigilante (Docente) ============");

        if ((d = selecionaDocente("Funcionários Docentes")) != null) {
            str = ".:Exames do(a) vigilante " + d.getNome() + ":.\n";
            System.out.print("\n" + str);
            for (Exame e : exames) {
                if (e.hasDocente(d.getNrMecan())) {
                    i++;
                    linha = i + ". " + e;
                    System.out.println(linha);
                    str += linha + "\n";
                }
            }
            if (i == 0) {
                System.out.println(d.getNome() + " não está convocado(a) para nenhum exame.");
            } else {
                guardaInfo(str);
            }
        }
        saiMenuAtual();

    }

    /**
     * Método que lista exames de um não docente
     */
    private static void listaExamesNaoDocente() {
        int i = 0;
        NaoDocente nd;
        String str, linha;

        System.out.println("\n============ Listar Exames de um Funcionário (Não Docente) ============");

        if ((nd = selecionaNaoDocente()) != null) {
            str = ".:Exames do(a) funcionário(a) " + nd.getNome() + ":." + "\n";
            System.out.print("\n" + str);
            for (Exame e : exames) {
                if (e.hasNaoDocente(nd.getNrMecan())) {
                    i++;
                    linha = i + ". " + e;
                    System.out.println(linha);
                    str += linha + "\n";
                }
            }
            if (i == 0) {
                System.out.println(nd.getNome() + " não está convocado(a) para nenhum exame.");

            } else {
                guardaInfo(str);
            }
        }
        saiMenuAtual();

    }

    /**
     * Método que lista as classificações de um exame
     */
    private static void listaNotasExame() {
        String str, linha;
        Exame e = selecionaExame();

        if (e != null) {
            if (!e.hasNotas()) {
                System.out.println("Este exame ainda não tem classificações atribuídas.");

            } else {
                str = "Exame: [" + e.toString() + "]";
                str += "\n  Nº Aluno\t\t\t Nome \t\t\t    Classificação";
                System.out.println(str);
                for (Map.Entry<Aluno, Float> a : e.getInscritos()) {
                    if (a.getValue() != null) {
                        linha = a.getKey().getNrAluno() + "\t\t" + a.getKey().getNome() + "\t\t" + a.getValue();
                    } else {
                        linha = a.getKey().getNrAluno() + "\t\t" + a.getKey().getNome();
                    }
                    str += "\n" + linha;
                    System.out.println(linha);
                }

                guardaInfo(str);
            }
        }
        saiMenuAtual();
    }

    /**
     * Método que carrega o ficheiro de texto para a lista Alunos
     *
     * @param ficheiro nome do ficheiro
     */
    private static void carregaFicheiroAlunos(String ficheiro) {

        String linha, nome, email, nrAluno, cursoID, anoMatricula, regime;
        Aluno novo;
        FicheiroTexto ft = new FicheiroTexto();
        try {
            ft.abreLeitura(ficheiro);
        } catch (FileNotFoundException ex) {
            return;
        }
        try {
            while ((linha = ft.leLinha()) != null) {
                StringTokenizer st = new StringTokenizer(linha, ";");
                nome = st.nextToken();
                email = st.nextToken();

                nrAluno = st.nextToken();
                cursoID = st.nextToken();
                anoMatricula = st.nextToken();
                regime = st.nextToken();

                Curso curso = getCursoPorID(Integer.parseInt(cursoID));

                novo = new Aluno(nome, email, Integer.parseInt(nrAluno), curso, Integer.parseInt(anoMatricula), regime);

                alunos.add(novo);
            }
        } catch (IOException ex) {
            System.err.println("Ocorreu uma exceção ao ler uma linha " + ex);
        }
        try {
            ft.fechaLeitura();
        } catch (IOException ex) {
            System.err.println("Ocorreu uma exceção ao fechar o ficheiro " + ex);
        }
    }

    /**
     * Método que carrega o ficheiro de texto para a lista Docentes
     *
     * @param ficheiro nome do ficheiro
     */
    private static void carregaFicheiroDocentes(String ficheiro) {
        String linha, nome, email, areaInv, nrMec, categ;
        Docente novo;

        FicheiroTexto ft = new FicheiroTexto();
        try {
            ft.abreLeitura(ficheiro);
        } catch (FileNotFoundException ex) {
            return;
        }
        try {
            while ((linha = ft.leLinha()) != null) {
                StringTokenizer st = new StringTokenizer(linha, ";");

                nome = st.nextToken();
                email = st.nextToken();

                nrMec = st.nextToken();
                categ = st.nextToken();
                areaInv = st.nextToken();

                novo = new Docente(nome, email, Integer.parseInt(nrMec), categ, areaInv);
                docentes.add(novo);
            }
        } catch (IOException ex) {
            System.err.println("Ocorreu uma exceção ao ler uma linha " + ex);
        }
        try {
            ft.fechaLeitura();
        } catch (IOException ex) {
            System.err.println("Ocorreu uma exceção ao fechar o ficheiro " + ex);
        }

    }

    /**
     * Método que carrega o ficheiro de texto para a lista NaoDocentes
     *
     * @param ficheiro nome do ficheiro
     */
    private static void carregaFicheiroNaoDocentes(String ficheiro) {

        String linha, nome, email, cargo, nrMec, categ;
        NaoDocente novo;
        FicheiroTexto ft = new FicheiroTexto();
        try {
            ft.abreLeitura(ficheiro);
        } catch (FileNotFoundException ex) {
            return;
        }
        try {
            while ((linha = ft.leLinha()) != null) {
                StringTokenizer st = new StringTokenizer(linha, ";");

                nome = st.nextToken();
                email = st.nextToken();

                nrMec = st.nextToken();
                categ = st.nextToken();
                cargo = st.nextToken();

                novo = new NaoDocente(nome, email, Integer.parseInt(nrMec), categ, cargo);
                naoDocentes.add(novo);
            }
        } catch (IOException ex) {
            System.err.println("Ocorreu uma exceção ao ler uma linha " + ex);
        }
        try {
            ft.fechaLeitura();
        } catch (IOException ex) {
            System.err.println("Ocorreu uma exceção ao fechar o ficheiro " + ex);
        }

    }

    /**
     * Método que carrega um ficheiro de objeto para um objeto
     *
     * @param ficheiro nome do ficheiro
     * @return objeto lido, caso o ficheiro exista
     */
    private static Object carregaFicheiroObjeto(String ficheiro) {
        Object obj;
        FicheiroObjeto fo = new FicheiroObjeto();
        try {
            if (fo.abreLeitura(ficheiro)) {
                obj = fo.leObjeto();
                fo.fechaLeitura();
                return obj;
            }
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Ocorreu uma exceção " + e);
        }
        return null;
    }

    /**
     * Método que atualiza/reescreve um ficheiro de objeto
     *
     * @param ficheiro nome do ficheiro
     * @param obj lista com cast do tipo Object
     */
    private static void atualizaFicheiroObjeto(String ficheiro, Object obj) {
        FicheiroObjeto fo = new FicheiroObjeto();
        try {
            fo.abreEscrita(ficheiro);
            fo.escreveObjeto(obj);
            fo.fechaEscrita();
        } catch (IOException e) {
            System.out.println("Ocorreu uma exceção " + e);
        }
    }

    /**
     * ***************************** Métodos Auxiliares
     */
    /**
     * Método que obtém elemento do tipo 'Curso' através do seu ID
     *
     * @param id códico único do curso
     * @return elemento do tipo 'Curso', caso exista
     */
    private static Curso getCursoPorID(int id) {
        for (Curso c : cursos) {
            if (c.getId() == id) {
                return c;
            }
        }

        return null;
    }

    /**
     * Método que verifica se uma dada String é convertível para Inteiro
     *
     * @param num String a avaliar
     * @return true (se for convertível) ou false (caso contrário)
     */
    private static boolean verificaNumero(String num) {
        try {
            Integer.parseInt(num);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Método que verifica se uma dada String é convertível para Float
     *
     * @param num String a avaliar
     * @return true (se for convertível) ou false (caso contrário)
     */
    private static boolean verificaFloat(String num) {
        try {
            Float.parseFloat(num);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Método que faz a transação de uma opção do Menu Principal para o próprio
     */
    private static void saiMenuAtual() {
        System.out.println("Prima qualquer tecla para voltar ao menu anterior.");
        sc.nextLine();
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    }

    /**
     * Método auxiliar que lista exames e permite ao utilizador selecionar um
     * deles
     *
     * @return Elemento do tipo 'Exame', caso seja selecionado
     */
    private static Exame selecionaExame() {
        int opcao = 0;
        String optStr;

        System.out.println("\n.:Exames:.");
        if (exames.isEmpty()) {
            System.out.println("Não existem exames na base de dados...");
            return null;
        }
        System.out.println("    Época\t    Disciplina\t\t    Data\t  Duração (min)\t\tSala\t\tDocente Responsável\t\t\tNº de Vigilantes\tNº de Funcionáriost\tNº de Inscritos ");
        for (Exame e : exames) {
            opcao++;
            System.out.println(opcao + ". " + e);
        }
        while (true) {
            System.out.println("Escolha uma opção: ");
            optStr = sc.nextLine();
            if (!verificaNumero(optStr)) {
                System.err.println("Erro: Opção tem de ser um número!");
            } else {
                opcao = Integer.parseInt(optStr);
                if (opcao > 0 && opcao <= exames.size()) {
                    return exames.get(opcao - 1);
                }
                if (opcao == 0) {
                    return null;
                }
                System.err.println("Erro: Opção inválida!");
            }
        }
    }

    /**
     * Método auxiliar que lista alunos admissíveis para um exame e permite ao
     * utilizador selecionar um deles
     *
     * @param e
     * @return Elemento do tipo 'Aluno', caso seja selecionado
     */
    private static Aluno selecionaAlunoExame(Exame e) {
        int opcao = 0;
        String optStr;
        boolean check = false;
        ArrayList<Integer> alunosInd = new ArrayList<>();

        System.out.println("\n.:Alunos Admissíveis:.");
        if (alunos.isEmpty()) {
            System.out.println("Não existem alunos na base de dados...");
            saiMenuAtual();
            return null;
        }
        for (Aluno a : alunos) {
            if (e.getDisciplina().hasAluno(a.getNrAluno())) {
                if (e.hasAdmissao(a)) {
                    if (!check) {
                        check = true;
                        System.out.println("   Nº Aluno\t\t\t Nome\t\t\t\t\tCurso\t\tAno de Matrícula\t\tRegime");

                    }
                    alunosInd.add(alunos.indexOf(a));
                    opcao++;
                    System.out.println(opcao + ". " + a);
                }
            }
        }
        if (opcao == 0) {
            System.out.println("Não existem alunos admissíveis na base de dados...");
            return null;
        }
        while (true) {
            System.out.println("Escolha uma opção: ");
            optStr = sc.nextLine();
            if (!verificaNumero(optStr)) {
                System.err.println("Erro: Opção tem de ser um número!");
            } else {
                opcao = Integer.parseInt(optStr);
                if (opcao > 0 && opcao <= alunosInd.size()) {
                    return alunos.get(alunosInd.get(opcao - 1));
                }
                if (opcao == 0) {
                    return null;
                }

                System.err.println("Erro: Opção inválida!");
            }
        }
    }

    /**
     * Método auxiliar que lista todos os alunos e permite ao utilizador
     * selecionar um deles
     *
     * @return Elemento do tipo 'Aluno', caso seja selecionado
     */
    private static Aluno selecionaAluno() {
        int opcao = 0;
        String optStr;

        System.out.println("\n.:Alunos:.");
        if (alunos.isEmpty()) {
            System.out.println("Não existem alunos na base de dados...");
            return null;
        }

        System.out.println("   Nº Aluno\t\t\t Nome\t\t\t\t\tCurso\t\tAno de Matrícula\t\tRegime");
        for (Aluno a : alunos) {
            opcao++;
            System.out.println(opcao + ". " + a);
        }
        while (true) {
            System.out.println("Escolha uma opção: ");
            optStr = sc.nextLine();
            if (!verificaNumero(optStr)) {
                System.err.println("Erro: Opção tem de ser um número!");
            } else {
                opcao = Integer.parseInt(optStr);
                if (opcao > 0 && opcao <= alunos.size()) {
                    return alunos.get(opcao - 1);
                }
                if (opcao == 0) {
                    return null;
                }
                System.err.println("Erro: Opção inválida!");
            }
        }

    }

    /**
     * Método auxiliar que permite ao utilicador selecionar um tipo de exame
     *
     * @return Opção escolhida
     */
    private static int selecionaTipoExame() {
        int opcao;
        String optStr;

        while (true) {
            System.out.println("\n.:Tipo de Exame:.\n"
                    + "1. Normal\n"
                    + "2. Recurso\n"
                    + "3. Especial\n"
                    + "0. Sair\n\n"
                    + "Escolha uma opção: ");
            optStr = sc.nextLine();
            if (!verificaNumero(optStr)) {
                System.err.println("Erro: Opção tem de ser um número!");
            } else {
                opcao = Integer.parseInt(optStr);
                if (opcao >= 0 && opcao < 4) {
                    return opcao;
                }
                System.err.println("Erro: Opcão inválida!");

            }
        }
    }

    private static Disciplina selecionaDiciplina() {
        int opcao = 0;
        String optStr;

        System.out.println("\n.:Disciplinas:.");
        if (disciplinas.isEmpty()) {
            System.out.println("Não existem disciplinas na base de dados...");
            return null;
        }
        System.out.println("  ID\t\tNome\t\tDocente Responsável\t\tNº de Docentes\t   Nº de Alunos");
        for (Disciplina d : disciplinas) {
            opcao++;
            System.out.println(opcao + ". " + d);
        }
        while (true) {
            System.out.println("Escolha uma opção: ");
            optStr = sc.nextLine();
            if (!verificaNumero(optStr)) {
                System.err.println("Erro: Opção tem de ser um número!");
            } else {
                opcao = Integer.parseInt(optStr);
                if (opcao > 0 && opcao <= disciplinas.size()) {
                    return disciplinas.get(opcao - 1);
                }
                if (opcao == 0) {
                    return null;
                }
                System.err.println("Erro: Opção inválida!");
            }
        }
    }

    private static Date selecionaData() {
        String dateStr;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        System.out.println("\n.:Data:.");
        while (true) {
            System.out.println("Insira uma data e hora [dd/MM/yyyy HH:mm]: ");
            dateStr = sc.nextLine();

            if (dateStr.trim().length() == sdf.toPattern().length()) {
                sdf.setLenient(false);
                try {
                    return sdf.parse(dateStr);
                } catch (ParseException e) {
                }
            }

            if (verificaNumero(dateStr)) {
                if (Integer.parseInt(dateStr) == 0) {
                    return null;
                }
            }
            System.err.println("Erro: Formato inválido!");
        }

    }

    private static boolean verificaDataExame(int id, Date data, int tipoExame) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        String ano = sdf.format(data);

        for (Exame e : exames) {
            if (sdf.format(e.getData()).equals(ano)) {
                if (e.getDisciplina().getId() == id) {
                    if (!e.validData(data, tipoExame)) {
                        return false;
                    }
                }
            } else {
                if (sdf.format(e.getData()).compareTo(ano) > 0) {
                    break;
                }
            }
        }

        return true;
    }

    private static ArrayList<Exame> getExamesSobrepostos(Date data, int duracao) {
        ArrayList<Exame> exSobrepostos = new ArrayList<>();

        exames.stream().filter((e) -> (e.isSobreposto(data, duracao))).forEachOrdered((e) -> {
            exSobrepostos.add(e);
        });

        return exSobrepostos;
    }

    private static Sala selecionaSala(ArrayList<Exame> examesSobrepostos) {
        int opcao = 0;
        String optStr;
        ArrayList<Integer> salasDisponiveisInd = new ArrayList<>();
        boolean free;

        System.out.println("\n.:Salas Disponíveis:.");

        for (Sala s : salas) {
            free = true;
            for (Exame e : examesSobrepostos) {
                if (e.hasSala(s.getId())) {
                    free = false;
                    break;
                }
            }
            if (free) {
                salasDisponiveisInd.add(salas.indexOf(s));
                opcao++;
                System.out.println(opcao + ". " + s);
            }
        }

        if (opcao == 0) {
            System.err.println("Não existem salas disponíveis para esta hora.Para alterar a data, crie de novo o exame.");
            saiMenuAtual();
            return null;
        }

        while (true) {
            System.out.println("Escolha uma opção: ");
            optStr = sc.nextLine();
            if (!verificaNumero(optStr)) {
                System.err.println("Erro: Opção tem de ser um número!");
            } else {
                opcao = Integer.parseInt(optStr);
                if (opcao > 0 && opcao <= salasDisponiveisInd.size()) {
                    return salas.get(salasDisponiveisInd.get(opcao - 1));
                }
                if (opcao == 0) {
                    return null;
                }
                System.err.println("Erro: Opção inválida!");
            }
        }
    }

    private static Docente selecionaDocente(String texto) {
        int opcao = 0;
        String optStr;

        System.out.println("\n.:" + texto + ":.");
        if (docentes.isEmpty()) {
            System.out.println("Não existem funcionários docentes na base de dados...");
            saiMenuAtual();
            return null;
        }
        System.out.println("  Nº Mencanográfico\t\t\tNome\t\t\tCategoria\t\tÁrea de Investigação");
        for (Docente d : docentes) {
            opcao++;
            System.out.println(opcao + ". " + d);
        }
        while (true) {
            System.out.println("Escolha uma opção: ");
            optStr = sc.nextLine();
            if (!verificaNumero(optStr)) {
                System.err.println("Erro: Opção tem de ser um número!");
            } else {
                opcao = Integer.parseInt(optStr);
                if (opcao > 0 && opcao <= docentes.size()) {
                    return docentes.get(opcao - 1);
                }
                if (opcao == 0) {
                    return null;
                }
                System.err.println("Erro: Opção inválida!");
            }
        }
    }

    private static NaoDocente selecionaNaoDocente() {
        int opcao = 0;
        String optStr;

        System.out.println("\n.:Funcionários Não Docentes:.");
        if (naoDocentes.isEmpty()) {
            System.out.println("Não existem funcionários não docentes na base de dados...");
            saiMenuAtual();
            return null;
        }
        System.out.println("  Nº Mencanográfico\t\t\tNome\t\t\tCategoria\t\t\tCargo");
        for (NaoDocente nd : naoDocentes) {
            opcao++;
            System.out.println(opcao + ". " + nd);
        }
        while (true) {
            System.out.println("Escolha uma opção: ");
            optStr = sc.nextLine();
            if (!verificaNumero(optStr)) {
                System.err.println("Erro: Opção tem de ser um número!");
            } else {
                opcao = Integer.parseInt(optStr);
                if (opcao > 0 && opcao <= naoDocentes.size()) {
                    return naoDocentes.get(opcao - 1);
                }
                if (opcao == 0) {
                    return null;
                }
                System.err.println("Erro: Opção inválida!");
            }
        }
    }

    private static void adicionaExame(Exame novo) {
        int i = 0;

        for (Exame e : exames) {
            if (e.getData().after(novo.getData())) {
                break;
            }
            i++;
        }

        exames.add(i, novo);
    }

    private static void guardaInfo(String str) {
        String optStr;
        FicheiroTexto ft = new FicheiroTexto();

        while (true) {
            System.out.println("Pretende guardar esta lista num ficheiro de texto? [s/n]");
            optStr = sc.nextLine();

            if (optStr.equalsIgnoreCase("s")) {
                System.out.println("Nome do ficheiro:");
                optStr = sc.nextLine();
                if (!optStr.endsWith(".txt")) {
                    optStr += ".txt";
                }

                try {
                    ft.abreEscrita(optStr);
                    ft.escreveLinha(str);
                    ft.fechaEscrita();
                } catch (IOException ex) {
                    System.err.println("Erro: Falha no ficheiro. Não será possível gravar.");
                }
                return;
            }
            if (optStr.equalsIgnoreCase("n")) {
                return;
            }

            System.err.println("Erro: Opção inválida!");
        }
    }

    private static void carregaTestes() {
        int i, j = 1, k=0;

        cursos.add(new Curso(1, "Engenharia Informatica", 3, "Licenciatura"));
        cursos.add(new Curso(2, "Design e Multimedia", 3, "Licenciatura"));

        carregaFicheiroAlunos("Alunos.txt");
        /*alunos.add(new Aluno("Ana Marta Moreira Raimundo", "anamraimundo@student.uc.pt", 2012365256, cursos.get(0), 3, "Normal"));
        alunos.add(new Aluno("Ana Margarida Moreira Raimundo", "anamraimundo@student.uc.pt", 2012365556, cursos.get(0), 2, "Trabalhador Estudante"));
        alunos.add(new Aluno("Ana Marta Moreira Ramadao", "anamraimundo@student.uc.pt", 2012365255, cursos.get(0), 1, "Normal"));
         */
        carregaFicheiroDocentes("Docentes.txt");
        /*
        docentes.add(new Docente("Ana Marta Moreira Raimundo", "anamraimundo@student.uc.pt", 2006108029, "Assistente", "Sistemas de Informacao"));
        docentes.add(new Docente("Joao Margarida Moreira Pereira", "anamraimundo@student.uc.pt", 1990108029, "Catedratico", "Engenharia de Software"));
         */

        carregaFicheiroNaoDocentes("NaoDocentes.txt");
        //naoDocentes.add(new NaoDocente("Joaquim Freitas da Silva", "anamraimundo@student.uc.pt", 1992108029, "Especialista de Informatica", "Apoio Tecnico"));

        ArrayList<Docente> temp = new ArrayList<>();
        ArrayList<Aluno> temp1 = new ArrayList<>();
        ArrayList<Aluno> temp2 = new ArrayList<>();
        
        for (Aluno a: alunos)
        {
            if (k%2 == 0)
                temp1.add(a);
            else
                temp2.add(a);
            
            k++;
        }

        for (i = 0; i < 3 * j; i++) {
            temp.add(docentes.get(i));
        }

        j++;
        disciplinas.add(new Disciplina(1, "POAO", temp.get(0), temp, temp1));
        temp = new ArrayList<>();

        for (; i < 3 * j; i++) {
            temp.add(docentes.get(i));
        }

        j++;

        disciplinas.add(new Disciplina(2, "SO", temp.get(0), temp, temp1));

        temp = new ArrayList<>();

        for (; i < 3 * j; i++) {
            temp.add(docentes.get(i));
        }

        j++;

        disciplinas.add(new Disciplina(3, "BD", temp.get(0), temp, temp1));

        temp = new ArrayList<>();

        for (; i < 3 * j; i++) {
            temp.add(docentes.get(i));
        }

        j++;
        disciplinas.add(new Disciplina(4, "CT", temp.get(0), temp, temp1));

        temp = new ArrayList<>();

        for (; i < 3 * j; i++) {
            temp.add(docentes.get(i));
        }

        j++;
        disciplinas.add(new Disciplina(5, "ED", temp.get(0), temp, temp1));

        temp = new ArrayList<>();

        for (; i < 3 * j; i++) {
            temp.add(docentes.get(i));
        }

        j++;
        disciplinas.add(new Disciplina(6, "DC", temp.get(0), temp, temp2));

        temp = new ArrayList<>();

        for (; i < 3 * j; i++) {
            temp.add(docentes.get(i));
        }

        j++;
        disciplinas.add(new Disciplina(7, "FC", temp.get(0), temp, temp2));

        temp = new ArrayList<>();

        for (; i < 3 * j; i++) {
            temp.add(docentes.get(i));
        }

        j++;
        disciplinas.add(new Disciplina(8, "IPRP", temp.get(0), temp, temp2));

        temp = new ArrayList<>();

        for (; i < 3 * j; i++) {
            temp.add(docentes.get(i));
        }

        j++;
        disciplinas.add(new Disciplina(9, "PD", temp.get(0), temp, temp2));
        temp = new ArrayList<>();

        for (; i < 3 * j; i++) {
            temp.add(docentes.get(i));
        }

        disciplinas.add(new Disciplina(10, "TWS", temp.get(0), temp, temp2));

        cursos.get(0).setDisciplina(disciplinas.get(0));
        cursos.get(0).setDisciplina(disciplinas.get(1));
        cursos.get(0).setDisciplina(disciplinas.get(2));
        cursos.get(0).setDisciplina(disciplinas.get(3));
        cursos.get(0).setDisciplina(disciplinas.get(4));
        cursos.get(1).setDisciplina(disciplinas.get(5));
        cursos.get(1).setDisciplina(disciplinas.get(6));
        cursos.get(1).setDisciplina(disciplinas.get(7));
        cursos.get(1).setDisciplina(disciplinas.get(8));
        cursos.get(1).setDisciplina(disciplinas.get(9));

        salas.add(new Sala(1, "EC", "Anf A"));
        salas.add(new Sala(2, "EC", "Anf B"));
        salas.add(new Sala(3, "DEI", "Torre B", 60));
        salas.add(new Sala(4, "DEI", "Torre D", 2, 20));
        salas.add(new Sala(5, "DEI", "Torre G", 5, 4, 30));

        cursos.add(new Curso(3, "Engenharia Eletrotecnica", 5, "Mestrado"));
        cursos.add(new Curso(4, "Direito", 4, "Licenciatura"));
        cursos.add(new Curso(5, "Direito", 2, "Mestrado"));
        cursos.add(new Curso(6, "Engenharia Informatica", 2, "Mestrado"));

        atualizaFicheiroObjeto("Cursos.dat", (Object) cursos);
        atualizaFicheiroObjeto("Disciplinas.dat", (Object) disciplinas);
        atualizaFicheiroObjeto("Salas.dat", (Object) salas);

    }

}
